WooCommerce Member Level Discount is a WooCommerce plugin to provide discount to customers based on their purchase history. 

The plugin allows you to generate multiple levels of membership to offer different discounts. These memberships are based on a specific duration of purchase history. It can be last week, last month or last year. 

An example scenario is that you want to offer 5% discount to all customers who have spent minimum 100$ in the last month. In addition to that, you want to offer 10% discount to customers who have spent minimum 200$. This plugin is to handle it. You can create multiple membership levels and offer different discounts. 

The plugin was based on a client's requirements. 