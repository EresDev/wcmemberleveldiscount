jQuery(document).ready(function ($) {
    $("#add-another-level").click(function () {
        var total = $(".levels_names").length + 1;
        $(".flag").before(
                '<tr><td>Membership Level ' + total + ': </td><td><input type="text" class="levels_names" name="level_names[]" placeholder="Level ' + total + ' Name" required></td><td>Starting Limit:</td><td><input type="number" name="level_history[]" onkeypress="return isNumberKey(event)" required> </td><td>Discount:</td><td><input type="number" onkeypress="return isNumberKey(event)" name="discount_level[]" step="0.01" required>%</td><td><textarea placeholder="Text for this level that will be displayed in My Account page. HTML is allowed." name="text_my_account[]"></textarea></td><td><label><input type="checkbox" class="remove_level" name="remove_level_' + total + '" />Remove</label></td></tr>'
                );


    });

    $("#wcmldiscount").click(function () {
        $(".remove_level:checked").parent().parent().parent().find("input").each(function () {
            $(this).removeAttr("required");
        });
    });
});
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
        return false;

    return true;
}