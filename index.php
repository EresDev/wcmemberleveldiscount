<?php
/*
  Plugin Name: WC Member Level Discount
  Description: WC Member Level Discount
  Version: 1.0
  Author: Muhammad Arslan Afzal
  Author URI: http://www.arslanafzal.com
 */

if (is_admin()) {
    
    add_action('admin_menu', 'WCMLDiscountMenu');

    function WCMLDiscountMenu() {
        add_options_page('Settings WCMLDiscount', 'WCML Discount', 'manage_options', 'wcmldiscount_admin.php', 'get_wcmldiscount_admin');
    }
   
    function get_wcmldiscount_admin() {
        if (isset($_POST['wcmldiscount'])) {

            delete_option("AresWCMLDiscount");
            $levels = array();
            foreach ($_POST['level_names'] as $k => $v) {

                if (isset($_POST['remove_level_' . ($k + 1)])) {
                    continue;
                }
                $levels[] = array(
                    'name' => $v,
                    'history' => $_POST['level_history'][$k],
                    'discount' => $_POST['discount_level'][$k],
                    'text_my_account' => $_POST['text_my_account'][$k],
                );
            }

            function cmp($a, $b) {
                return $a['history'] - $b['history'];
            }

            usort($levels, "cmp");
            $options = array(
                'duration' => $_POST['days_history'],
                'levels' => $levels,
                'cart_discount_text' => $_POST['cart_discount_text'],
                'text_my_account_new' => $_POST['text_my_account_new'],
            );
            add_option("AresWCMLDiscount", $options);
        }
        wp_enqueue_script('admin-js', plugin_dir_url(__FILE__) . 'js/admin.js', array('jquery'));
        echo '<h1>WooCommerce Member Level Discount</h1><hr>';
        ?>
        <form method="post">
            <table class="levels">
                <?php
                $options = get_option("AresWCMLDiscount");

                if ($options && count($options['levels']) != 0) {
                    foreach ($options['levels'] as $i => $level) {
                        echo '<tr><td>Membership Level ' . ($i+1) . ': </td><td><input type="text" class="levels_names" name="level_names[]" placeholder="Level 1 Name" value="' . $level['name'] . '" required></td><td>Starting Limit:</td><td><input type="number" name="level_history[]" onkeypress="return isNumberKey(event)"  value="' . $level['history'] . '" required> </td><td>Discount:</td><td><input type="number" name="discount_level[]" step="0.01" onkeypress="return isNumberKey(event)"  value="' . $level['discount'] . '"  required>%</td><td><textarea placeholder="Text for this level that will be displayed in My Account page. HTML is allowed." name="text_my_account[]">'.(isset($level['text_my_account'])?$level['text_my_account']:"").'</textarea></td><td><label><input type="checkbox" name="remove_level_' . ($i + 1) . '"  class="remove_level" />Remove</label></td></tr>';
                    }
                } else {
                    ?>
                <tr><td>Membership Level 1: </td><td><input type="text" class="levels_names" name="level_names[]" placeholder="Level 1 Name" required></td><td>Starting Limit:</td><td><input type="number" onkeypress="return isNumberKey(event)"  name="level_history[]" required> </td><td>Discount:</td><td><input type="number" name="discount_level[]" step="0.01" onkeypress="return isNumberKey(event)" required>%</td><td><textarea placeholder="Text for this level that will be displayed in My Account page. HTML is allowed." name="text_my_account[]"></textarea></td><td><label><input type="checkbox" name="remove_level_1"  class="remove_level" />Remove</label></td></tr>
                <?php } ?>
                <tr class="flag"><td colspan="8"><input type="button" value="Add Another Level" class="button" id="add-another-level"/></td></tr>
                <tr><td>Based on Purchase History of: </td><td colspan="7"><input type="number" name="days_history" onkeypress="return isNumberKey(event)" value="<?php echo (isset($options['duration']) ? $options['duration'] : ""); ?>" required> Days</td></tr>
                <tr><td>Cart Discount Text: </td><td colspan="7"><input type="text" name="cart_discount_text" value="<?php echo (isset($options['cart_discount_text']) ? $options['cart_discount_text'] : "Membership Discount"); ?>" required></td></tr>
                <tr><td>My Account Text for New Users: </td><td colspan="7"><textarea name="text_my_account_new" placeholder="HTML is allowed."><?php echo (isset($options['text_my_account_new']) ? $options['text_my_account_new'] : ""); ?></textarea></td></tr>
                
                <tr><td colspan="8"><input type="submit"  id="wcmldiscount"  name="wcmldiscount" value="Save Changes" class="button button-primary button-large"/></td></tr>

            </table>
        </form>
        <?php
    }

}


add_action('woocommerce_cart_calculate_fees', 'woocommerce_custom_discount');

function woocommerce_custom_discount() {
    $options = get_option("AresWCMLDiscount");
    $discount = 0;
    if ($options && count($options['levels']) != 0) {
        global $woocommerce;
        global $wpdb;
        $querystr = "select sum({$wpdb->prefix}woocommerce_order_itemmeta.meta_value) as total from {$wpdb->prefix}woocommerce_order_itemmeta inner join {$wpdb->prefix}woocommerce_order_items on {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id = {$wpdb->prefix}woocommerce_order_items.order_item_id inner join {$wpdb->prefix}posts on {$wpdb->prefix}posts.ID = {$wpdb->prefix}woocommerce_order_items.order_id inner join {$wpdb->prefix}postmeta on {$wpdb->prefix}postmeta.post_id = {$wpdb->prefix}posts.ID where ({$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '_line_total' or {$wpdb->prefix}woocommerce_order_itemmeta.meta_key='cost') and {$wpdb->prefix}postmeta.meta_key = '_customer_user' and {$wpdb->prefix}postmeta.meta_value='" . get_current_user_id() . "' and DATEDIFF(NOW(), {$wpdb->prefix}posts.post_date) <= {$options['duration']}";
        $result = $wpdb->get_results($querystr, OBJECT);
        $result = $result[0];
        if($result->total){
            foreach ($options['levels'] as $i => $level) {
                
                if($level["history"] > $result->total){
                    break;
                }
            } 
            if($options['levels'][$i]["history"] > $result->total){
                --$i;
            }
            $percentage = $options['levels'][$i]["discount"];
            
            $discount = ($woocommerce->cart->cart_contents_total * $percentage / 100);    
        }
    }
    
    
    $woocommerce->cart->add_fee(($options["cart_discount_text"]?$options["cart_discount_text"]:"Membership Discount"), -$discount, true, 'standard');
}


add_shortcode("WCMLDiscount_my_account", "getWCMLDiscount_my_account");
function getWCMLDiscount_my_account(){
    $options = get_option("AresWCMLDiscount");
    
    if ($options && count($options['levels']) != 0) {
       
        global $wpdb;
        $querystr = "select sum({$wpdb->prefix}woocommerce_order_itemmeta.meta_value) as total from {$wpdb->prefix}woocommerce_order_itemmeta inner join {$wpdb->prefix}woocommerce_order_items on {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id = {$wpdb->prefix}woocommerce_order_items.order_item_id inner join {$wpdb->prefix}posts on {$wpdb->prefix}posts.ID = {$wpdb->prefix}woocommerce_order_items.order_id inner join {$wpdb->prefix}postmeta on {$wpdb->prefix}postmeta.post_id = {$wpdb->prefix}posts.ID where ({$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '_line_total' or {$wpdb->prefix}woocommerce_order_itemmeta.meta_key='cost') and {$wpdb->prefix}postmeta.meta_key = '_customer_user' and {$wpdb->prefix}postmeta.meta_value='" . get_current_user_id() . "' and DATEDIFF(NOW(), {$wpdb->prefix}posts.post_date) <= {$options['duration']}";
        $result = $wpdb->get_results($querystr, OBJECT);
        $result = $result[0];
        if($result->total){
            foreach ($options['levels'] as $i => $level) {
                
                if($level["history"] > $result->total){
                    break;
                }
            } 
            if($options['levels'][$i]["history"] > $result->total){
                --$i;
            }
            return $options['levels'][$i]["text_my_account"];
        }
        else {
            return $options['text_my_account_new'];
        }
    }
    
}

add_shortcode("WCMLDiscount", "getWCMLDiscount");

function getWCMLDiscount($action){
    $options = get_option("AresWCMLDiscount");
    
    if ($options && count($options['levels']) != 0) {
       
        global $wpdb;
        $querystr = "select sum({$wpdb->prefix}woocommerce_order_itemmeta.meta_value) as total from {$wpdb->prefix}woocommerce_order_itemmeta inner join {$wpdb->prefix}woocommerce_order_items on {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id = {$wpdb->prefix}woocommerce_order_items.order_item_id inner join {$wpdb->prefix}posts on {$wpdb->prefix}posts.ID = {$wpdb->prefix}woocommerce_order_items.order_id inner join {$wpdb->prefix}postmeta on {$wpdb->prefix}postmeta.post_id = {$wpdb->prefix}posts.ID where ({$wpdb->prefix}woocommerce_order_itemmeta.meta_key = '_line_total' or {$wpdb->prefix}woocommerce_order_itemmeta.meta_key='cost') and {$wpdb->prefix}postmeta.meta_key = '_customer_user' and {$wpdb->prefix}postmeta.meta_value='" . get_current_user_id() . "' and DATEDIFF(NOW(), {$wpdb->prefix}posts.post_date) <= {$options['duration']}";
        $result = $wpdb->get_results($querystr, OBJECT);
        $result = $result[0];
        if($result->total){
            foreach ($options['levels'] as $i => $level) {
                
                if($level["history"] > $result->total){
                    break;
                }
            } 
            if($options['levels'][$i]["history"] > $result->total){
                --$i;
            }
            
        }
        
    }
    switch($action['action']){
        case 'current_level':
            
            if($result->total){
                return $options['levels'][$i]["name"];
            }
                
            
            break;
        case 'current_purchase':
            return (float)$result->total;
            break;
        
        case 'next_level_after':
            if($result->total){
                if($options['levels'][$i+1]["history"] - $result->total < 0 ){
                    return 0;
                }
                return ($options['levels'][$i+1]["history"] - $result->total);
            }
            else if($options['levels'][0]["history"] == 0){
                return ($options['levels'][1]["history"]-$options['levels'][0]["history"]);
            }
            break;
    }
}